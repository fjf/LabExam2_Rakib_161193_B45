-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 01, 2017 at 11:15 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atomic_me`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE `birthday` (
  `id` int(11) NOT NULL,
  `birthday` date NOT NULL,
  `author_name` varchar(200) NOT NULL,
  `soft_deleted` varchar(200) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `birthday`, `author_name`, `soft_deleted`) VALUES
(29, '1974-10-07', 'Mustafa Rakib', 'No'),
(31, '1980-02-19', 'Tushar Shuvro', 'No'),
(33, '1996-06-12', 'Shahariar Shoaib', 'No'),
(34, '2017-01-28', 'a', 'No'),
(35, '2017-01-14', 'r', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE `book_title` (
  `id` int(11) NOT NULL,
  `book_name` varchar(111) NOT NULL,
  `author_name` varchar(111) NOT NULL,
  `soft_deleted` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_name`, `author_name`, `soft_deleted`) VALUES
(1, 'Himu', 'Humayun Ahmed', 'Yes'),
(3, 'fdssf', 'fdgfdh fh ht', 'No'),
(4, 'fdssf', 'fdgfdh fh ht', 'No'),
(5, 'gfhfh', 'fghfdh', 'No'),
(6, 'dsfdgdg', 'fdgdg', 'Yes'),
(7, 'fdzdsf g', 'dgf dg dr t', 'No'),
(8, 'fdzdsf g', 'dgf dg dr t', 'No'),
(9, 'sfd s', 'sdf ser ', 'No'),
(10, 'sfd s', 's gdh try', 'No'),
(11, 'dfssf', 'sfd sf', 'Yes'),
(12, 'dfssf', 'sfd sf', 'No'),
(13, 'fb', 'xcgf', 'No'),
(14, 'dfgfdhg ', 'dfg fjht yu', 'No'),
(15, 'dfgfdhg ', 'dfg fjht yu', 'No'),
(16, 'Himu', 'Humayun Ahmed', 'No'),
(17, 'Himu', 'Humayun Ahmed', 'No'),
(18, 'Himu', 'Humayun Ahmed', 'No'),
(19, 'Himu', 'Humayun Ahmed', 'No'),
(20, 'dsfsf', 'sdfsf', 'No'),
(21, 'fdgdgfdg', '36543325', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `city` varchar(200) NOT NULL,
  `author_name` varchar(200) NOT NULL,
  `soft_deleted` varchar(200) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `city`, `author_name`, `soft_deleted`) VALUES
(13, 'Chittagong', 'Mustafa Rakib', 'No'),
(14, 'Dhaka', 'Shahariar Shoaib', 'No'),
(15, 'Dhaka', 'g', 'No'),
(16, 'Dhaka', 'w', 'No'),
(17, 'Dhaka', 'l', 'No'),
(18, 'Dhaka', 'l', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `id` int(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `author_name` varchar(200) NOT NULL,
  `soft_deleted` varchar(200) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `email`, `author_name`, `soft_deleted`) VALUES
(13, 'rakib242@gmail.com', 'Mustafa Rakib', 'No'),
(14, 'df@gmail.com', 'fg', 'No'),
(15, 'shoapan@yahoo.com', 'Shahariar Shoaib', 'No'),
(16, 'l@gmail.com', 'l', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE `gender` (
  `id` int(200) NOT NULL,
  `gender` varchar(200) NOT NULL,
  `author_name` varchar(200) NOT NULL,
  `soft_deleted` varchar(200) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `gender`, `author_name`, `soft_deleted`) VALUES
(21, 'Male', 'Mustafa Rakib', 'No'),
(22, 'Female', 'Rania Zannat', 'No'),
(23, 'Male', 'Shahariar Shoaib', 'No'),
(24, 'Female', 'Afsana Ruby', 'No'),
(25, 'Female', 'j', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE `hobbies` (
  `id` int(200) NOT NULL,
  `hobbies` varchar(200) NOT NULL,
  `author_name` varchar(200) NOT NULL,
  `soft_deleted` varchar(200) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`id`, `hobbies`, `author_name`, `soft_deleted`) VALUES
(11, 'programming', 'asdfghjk', 'No'),
(12, 'fishing,hacking', 'dfghjkl', 'No'),
(13, 'programming', 'Mustafa Rakib', 'No'),
(14, 'programming,hacking,playing', 'Tushar Shuvro', 'No'),
(15, 'fishing,gardening', 'Shahariar Shoaib', 'No'),
(16, 'fishing,gardening', 'Afsana Ruby', 'No'),
(17, 'fishing', 's', 'No'),
(18, 'programming', 'k', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `profile_picture`
--

CREATE TABLE `profile_picture` (
  `id` int(200) NOT NULL,
  `profile_picture` varchar(230) NOT NULL,
  `author_name` varchar(200) NOT NULL,
  `soft_deleted` varchar(200) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile_picture`
--

INSERT INTO `profile_picture` (`id`, `profile_picture`, `author_name`, `soft_deleted`) VALUES
(15, '1485683809pic.jpg', 'Mustafa Rakib', 'No'),
(17, '1485734111pic2.jpg', 'Rakib Hasan', 'No'),
(18, '1485738247pic2.jpg', 'Reckob Hans', 'No'),
(19, '1485756201bitm.jpg', 'bitm seip', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `summary_of_organization`
--

CREATE TABLE `summary_of_organization` (
  `id` int(200) NOT NULL,
  `summary_of_organization` varchar(200) NOT NULL,
  `author_name` varchar(200) NOT NULL,
  `soft_deleted` varchar(200) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `summary_of_organization`
--

INSERT INTO `summary_of_organization` (`id`, `summary_of_organization`, `author_name`, `soft_deleted`) VALUES
(11, 'yjtyuhjj', 'jyuyjtj', 'No'),
(12, 'sdfghjk', 'asdfghjkl', 'No'),
(13, 'This is the summary of our organization', 'Mustafa Rakib', 'No'),
(14, 'Another summary of organization', 'Shahariar Shoaib', 'No'),
(15, 'dfgdfg ghfghf dghfghfg ghfghfgh', 'Afsana Ruby', 'No'),
(16, 'kjhfksdfhj  sdkjhskdfh  ksdksjhfk', 'Afsana Ruby', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(111) NOT NULL,
  `last_name` varchar(111) NOT NULL,
  `email` varchar(111) NOT NULL,
  `password` varchar(111) NOT NULL,
  `phone` varchar(111) NOT NULL,
  `address` varchar(333) NOT NULL,
  `email_verified` varchar(111) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `phone`, `address`, `email_verified`) VALUES
(17, 'Test', 'User', 'tushar.chowdhury@gmail.com', '202cb962ac59075b964b07152d234b70', '01711111111', 'Chittagong', 'Yes'),
(18, 'sdjf', 'lksdjf', 'tusharbd@gmail.com', 'b518d60581cfcd1c861145739d4666d6', '5235', 'dfgdg', 'Yes'),
(19, 'asfds', 'sdfgs', 'x@y.z', '202cb962ac59075b964b07152d234b70', '4545', 'sfsj', '4ae15d1c46f25be8db9d07061463c5f0'),
(24, 'Istiyak', 'Amin', 'istiyakaminsanto@yahoo.com', '4a7d1ed414474e4033ac29ccb8653d9b', '00', 'a', 'Yes');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthday`
--
ALTER TABLE `birthday`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobbies`
--
ALTER TABLE `hobbies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_picture`
--
ALTER TABLE `profile_picture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `summary_of_organization`
--
ALTER TABLE `summary_of_organization`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthday`
--
ALTER TABLE `birthday`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `hobbies`
--
ALTER TABLE `hobbies`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `profile_picture`
--
ALTER TABLE `profile_picture`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `summary_of_organization`
--
ALTER TABLE `summary_of_organization`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
