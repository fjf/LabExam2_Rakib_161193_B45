<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Profile Picture Create form</title>
    <link rel="stylesheet" href="../../../font-awesome/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Yellowtail" rel="stylesheet">
    <link rel="stylesheet" href="../../../resource/assets/css/bootstrap.css">
    <link rel="stylesheet" href="../../../resource/assets/css/bootstrap-responsive.css">
    <link rel="stylesheet" href="../../../resource/assets/css/cssanimation.css">
    <link rel="stylesheet" href="../../../resource/assets/style.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
</head>

<body>
<div class="container">
    <div class="container-fluid">
        <header>
            <div class="logo_area">
                <p class="logo cssanimation fadeInLeft">Atomic Project</p>
                <h3 class="subLogo cssanimation fadeInRight">SEIP-161193 Batch-45</h3>
            </div>
            <nav class="navbar navbar-inverse">
                <ul class="nav nav-pills">
                    <li><a href="../index.php">HOME</a></li>
                    <li><a href="../Birthday/index.php">BIRTHDAY</a></li>
                    <li><a href="../BookTitle/index.php">BOOK TITLE</a></li>
                    <li><a href="../City/index.php">CITY</a></li>
                    <li><a href="../Email/index.php">EMAIL</a></li>
                    <li><a href="../Gender/index.php">GENDER</a></li>
                    <li><a href="../Hobbies/index.php">HOBBY</a></li>
                    <li class="active"><a href="../ProfilePicture/index.php">PROFILE PIC</a></li>
                    <li><a href="../SummaryOfOrganization/index.php">SUMMARY OF ORG.</a></li>
                </ul>
            </nav>
        </header>

        <div><img src="../../../resource/img/pic.jpeg" alt=""></div>
        <p class="title">Profile Picture</p>

        <?php
            require_once("../../../vendor/autoload.php");
            use App\Message\Message;

            if(!isset($_SESSION)){
                session_start();
            };
            $msg = Message::getMessage();
            echo "<div id='message'> $msg </div>";

        $objProfilePicture = new \App\BookTitle\BookTitle();
        $objProfilePicture->setData($_GET);
        $oneData = $objProfilePicture->view();
        ?>

        <fieldset class="control-group ">
            <legend>Add your Profile Picture</legend>
            <form action="store.php" method="post" class="form-inline" enctype="multipart/form-data">
                <div class="form-group input_form ">
                    <label for="exampleInputfile1">Upload Profile Picture</label>
                    <input type="file" name="profile_picture" id="exampleInputfile1" class="form-control" style="background: white;border-radius: 3px;padding-left: 5px">
                </div>
                <div class="input_form">
                    <label for="Author">User Name</label>
                    <input type="text" id="Author" class="input-xxlarge" name="author_name">
                </div>
                <div>
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                    <input type="submit" class="btn btn-success" value="submit">
                </div>
            </form>
        </fieldset>

    </div>
<footer class="modal-footer">&copy; 2017 PHP Project. Mustafa Rakib / All rights reserved.</footer>
</div>

<script src="../../../resource/bootstrap/js/jquery.js"></script>
<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>

</body>
</html>
