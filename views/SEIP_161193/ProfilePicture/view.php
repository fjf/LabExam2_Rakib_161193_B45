<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Profile Picture - Active List</title>
    <link rel="stylesheet" href="../../../font-awesome/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Yellowtail" rel="stylesheet">
    <link rel="stylesheet" href="../../../resource/assets/css/bootstrap.css">
    <link rel="stylesheet" href="../../../resource/assets/css/bootstrap-responsive.css">
    <link rel="stylesheet" href="../../../resource/assets/css/cssanimation.css">
    <link rel="stylesheet" href="../../../resource/assets/style.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <style>
        td{ background: #9cc2cb;
    </style>
</head>

<body>
<div class="container">
    <div class="container-fluid">
        <header>
            <div class="logo_area">
                <p class="logo cssanimation fadeInLeft">Atomic Project</p>
                <h3 class="subLogo cssanimation fadeInRight">SEIP-161193 Batch-45</h3>
            </div>
            <nav class="navbar navbar-inverse">
                <ul class="nav nav-pills">
                    <li><a href="../index.php">HOME</a></li>
                    <li><a href="../Birthday/index.php">BIRTHDAY</a></li>
                    <li><a href="../BookTitle/index.php">BOOK TITLE</a></li>
                    <li><a href="../City/index.php">CITY</a></li>
                    <li><a href="../Email/index.php">EMAIL</a></li>
                    <li><a href="../Gender/index.php">GENDER</a></li>
                    <li><a href="../Hobbies/index.php">HOBBY</a></li>
                    <li class="active"><a href="../ProfilePicture/index.php">PROFILE PIC</a></li>
                    <li><a href="../SummaryOfOrganization/index.php">SUMMARY OF ORG.</a></li>
                </ul>
            </nav>
        </header>

        <div><img src="../../../resource/img/pic.jpeg" alt=""></div></div></div>

<?php
require_once("../../../vendor/autoload.php");

$objProfilePicture = new \App\ProfilePicture\ProfilePicture();
$objProfilePicture->setData($_GET);
$oneData = $objProfilePicture->view();

echo "
  <div class='container'>
    <h1 style='text-align: center'> Single ProfilePicture Information </h1>

    <table class='table table - striped table - bordered' cellspacing='0px' style='width: 90%'>
       <tr>
           <td>ID: </td>
           <td> $oneData->id </td>
       </tr>
       <tr>
           <td>Profile Picture: </td>
           <td> $oneData->profile_picture </td>
       </tr>
       <tr>
           <td>User Name: </td>
           <td> $oneData->author_name </td>
       </tr>
    </table>
  </div>
";

?>
<footer class="modal-footer" style="width: 96%">&copy; 2017 PHP Project. Mustafa Rakib / All rights reserved.</footer>

</body>
</html>
