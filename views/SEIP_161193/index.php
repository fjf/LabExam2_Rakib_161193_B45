<?php
    include_once('../../vendor/autoload.php');

    if(!isset($_SESSION) )session_start();
    use App\Message\Message;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>index</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="../../font-awesome/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Yellowtail" rel="stylesheet">
    <link rel="stylesheet" href="../../resource/assets/css/bootstrap.css">
    <link rel="stylesheet" href="../../resource/assets/js/bootstrap.min.js">
    <link rel="stylesheet" href="../../resource/assets/css/bootstrap-responsive.css">
    <link rel="stylesheet" href="../../resource/assets/css/cssanimation.css">
    <link rel="stylesheet" href="ism/css/my-slider.css"/>
    <script src="ism/js/ism-2.2.min.js"></script>
    <link rel="stylesheet" href="../../resource/assets/style.css">
    <link rel="stylesheet" href="../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../resource/bootstrap/js/bootstrap.min.js"></script>
</head>

<body>
<div class="container">
    <div class="container-fluid">
        <header>
            <div class="logo_area">
                <p class="logo cssanimation fadeInLeft">Atomic Project</p>
                <h3 class="subLogo cssanimation fadeInRight">SEIP-161193 Batch-45</h3>
            </div>
            <nav class="navbar navbar-inverse">
                <ul class="nav nav-pills">
                    <li class="active"><a href="index.php">HOME</a></li>
                    <li><a href="Birthday/index.php">BIRTHDAY</a></li>
                    <li><a href="BookTitle/index.php">BOOKTITLE</a></li>
                    <li><a href="City/index.php">CITY</a></li>
                    <li><a href="Email/index.php">EMAIL</a></li>
                    <li><a href="Gender/index.php">GENDER</a></li>
                    <li><a href="Hobbies/index.php">HOBBY</a></li>
                    <li><a href="ProfilePicture/index.php">PROFILE PIC</a></li>
                    <li><a href="SummaryOfOrganization/index.php">SUMMARY OF ORG.</a></li>
                </ul>
            </nav>
        </header>

        <div><img src="../../resource/img/bitm.jpg" alt=""></div>

        <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
        echo "&nbsp;".Message::message();
        }
        Message::message(NULL);
        ?>
    <div>
<footer class="modal-footer">&copy; 2017 PHP Project. Mustafa Rakib / All rights reserved.</footer>
</div>

</body>
</html>
