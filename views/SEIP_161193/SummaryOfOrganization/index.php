<?php
require_once("../../../vendor/autoload.php");

$objSummaryOfOrganization = new \App\SummaryOfOrganization\SummaryOfOrganization();
$allData = $objSummaryOfOrganization->index();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>BSummary Of Organization - Active List</title>
    <link rel="stylesheet" href="../../../font-awesome/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Yellowtail" rel="stylesheet">
    <link rel="stylesheet" href="../../../resource/assets/css/bootstrap.css">
    <link rel="stylesheet" href="../../../resource/assets/css/bootstrap-responsive.css">
    <link rel="stylesheet" href="../../../resource/assets/css/cssanimation.css">
    <link rel="stylesheet" href="../../../resource/assets/style.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <div class="container-fluid">
        <header>
            <div class="logo_area">
                <p class="logo cssanimation fadeInLeft">Atomic Project</p>
                <h3 class="subLogo cssanimation fadeInRight">SEIP-161193 Batch-45</h3>
            </div>
            <nav class="navbar navbar-inverse">
                <ul class="nav nav-pills">
                    <li><a href="../index.php">HOME</a></li>
                    <li><a href="../Birthday/index.php">BIRTHDAY</a></li>
                    <li><a href="../BookTitle/index.php">BOOK TITLE</a></li>
                    <li><a href="../City/index.php">CITY</a></li>
                    <li><a href="../Email/index.php">EMAIL</a></li>
                    <li><a href="../Gender/index.php">GENDER</a></li>
                    <li><a href="../Hobbies/index.php">HOBBY</a></li>
                    <li><a href="../ProfilePicture/index.php">PROFILE PIC</a></li>
                    <li class="active"><a href="../SummaryOfOrganization/index.php">SUMMARY OF ORG.</a></li>
                </ul>
            </nav>
        </header>

        <div><img src="../../../resource/img/org.jpg" alt=""></div>
        <p class="title">Summary Of Organization - Active List</p>

<div class="content">
    <?php
    require_once("../../../vendor/autoload.php");

    if(!isset($_SESSION)) session_start();
    use App\Message\Message;

    $msg = Message::message();
    echo "<div id='message' style='color: green;font-weight: bold'>  $msg </div>";
    ?>

    <table class="table table-bordered table-hover">
        <tr class="warning" style="color: #A0522D;">
            <td style='width: 10%; text-align: center'>SERIAL</td>
            <td style='width: 10%; text-align: center'>ID</td>

            <td>SUMMARY OF ORGANIZATION</td>
            <td>ORGANIZATION NAME</td>
            <td>ACTION BUTTONS</td>
        </tr>

        <?php
        $serial= 1;
        foreach($allData as $oneData){
            if($serial%2) $bgColor = "success";
            else $bgColor = "warning";

        echo "
              <tr class=\"$bgColor\">
                 <td style='width: 10%; text-align: center'>$serial</td>
                 <td style='width: 10%; text-align: center'>$oneData->id</td>
                 <td>$oneData->summary_of_organization</td>
                 <td>$oneData->author_name</td>
                 <td>
                    <a href='view.php?id=$oneData->id' class='btn btn-info'>View</a>
                    <a href='edit.php?id=$oneData->id' class='btn btn-primary'>Edit</a>
                 </td>
              </tr>";
            $serial++;
        }
        ?>

    </table>
</div>
</div>
<footer class="modal-footer">&copy; 2017 PHP Project. Mustafa Rakib / All rights reserved.</footer>
</div>

<script src="../../../resource/bootstrap/js/jquery.js"></script>
<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>

</body>
</html>
