<?php
namespace App\ProfilePicture;

use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;

class ProfilePicture extends DB
{
    private $id;
    private $profile_picture;
    private $author_name;

    public function setData($allPostData = NULL)
    {
        if (array_key_exists('id', $allPostData)) {
            $this->id = $allPostData['id'];
        }

        if (array_key_exists('img_name', $allPostData)) {
            $this->profile_picture = $allPostData['img_name'];
        }

        if (array_key_exists('author_name', $allPostData)) {
            $this->author_name = $allPostData['author_name'];
        }
    }

    public function store(){
        $arrData = array($this->profile_picture, $this->author_name);

        $query = "Insert INTO profile_picture(profile_picture, author_name) VALUES (?,?)";
        $STH = $this->DBH->prepare($query);

        $result = $STH->execute($arrData);

        if ($result)
            Message::message("<h1 style='text-align: center;color: red'>Success! Data Has Been Inserted Successfully :)</h1>");
        else
            Message::message("<h3>Failed! Data Has Not Been Inserted :( </h3>");

        Utility::redirect('index.php');
    }

    public function view(){
        $sql = "Select * from profile_picture where id=".$this->id;
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }

    public function index(){
        $sql = "Select * from profile_picture where soft_deleted='No'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
}
