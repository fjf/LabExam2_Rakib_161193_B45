<?php
namespace App\Hobbies;

use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;

class Hobbies extends DB
{
    private $id;
    private $hobbies;
    private $author_name;

    public function setData($allPostData = NULL)
    {
        if (array_key_exists('id', $allPostData)) {
            $this->id = $allPostData['id'];
        }

        if (array_key_exists('hobbies', $allPostData)) {
            $imp = implode(",", $allPostData['hobbies']);
            $this->hobbies = $imp;
        }

        if (array_key_exists('author_name', $allPostData)) {
            $this->author_name = $allPostData['author_name'];
        }
    }

    public function store(){
        $arrData = array($this->hobbies, $this->author_name);

        $query = "Insert INTO hobbies(hobbies, author_name) VALUES (?,?)";
        $STH = $this->DBH->prepare($query);

        $result = $STH->execute($arrData);

        if ($result)
            Message::message("<h1 style='text-align: center;color: red'>Success! Data Has Been Inserted Successfully :)</h1>");
        else
            Message::message("<h3>Failed! Data Has Not Been Inserted :( </h3>");

        Utility::redirect('index.php');
    }

    public function view(){
        $sql = "Select * from hobbies where id=".$this->id;
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }

    public function index(){
        $sql = "Select * from hobbies where soft_deleted='No'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
}
